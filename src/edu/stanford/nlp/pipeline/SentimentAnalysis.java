/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stanford.nlp.pipeline;

import eu.mico.platform.event.api.AnalysisResponse;
import eu.mico.platform.event.api.AnalysisService;
import eu.mico.platform.event.api.EventManager;
import eu.mico.platform.event.impl.EventManagerImpl;
import eu.mico.platform.event.model.AnalysisException;
import eu.mico.platform.persistence.model.Content;
import eu.mico.platform.persistence.model.ContentItem;
import org.apache.commons.io.IOUtils;
import org.openrdf.model.URI;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.DCTERMS;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;
import java.util.*;
import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.trees.TreeCoreAnnotations.*; 
import edu.stanford.nlp.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author yonasd
 */
public class SentimentAnalysis implements AnalysisService {

    private static Logger log = LoggerFactory.getLogger(SentimentAnalysis.class);

    private static SimpleDateFormat isodate = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'", DateFormatSymbols.getInstance(Locale.US));
    static {
        isodate.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public URI getServiceID() {
        return new URIImpl("http://www.mico-project.org/services/SentimentAnalysis");
    }

    @Override
    public String getProvides() {
        return "text/xml";
    }

    @Override
    public String getRequires() {
        return "text/plain";
    }

    @Override
    public String getQueueName() {
        return "SentimentAnalysis";
    }

    @Override
    public void call(AnalysisResponse analysisResponse, ContentItem contentItem, URI uri) throws AnalysisException, IOException {
        try {
            // get the content part with the given URI
            Content content = contentItem.getContentPart(uri);

            // get the input stream and read it into a string
            String text = IOUtils.toString(content.getInputStream(), "utf-8");

        PrintWriter xmlOut = new PrintWriter("SentimentxmlOutput.xml");
        Properties props = new Properties(); 
        props.setProperty("annotators","tokenize, ssplit, cleanxml,pos, parse,sentiment"); 
        props.setProperty("ssplit.newlineIsSentenceBreak", "always");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props); 
        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation); 
        pipeline.xmlPrint(annotation, xmlOut);
        
            // create a new content part for assigning the metadata
            Content result = contentItem.createContentPart();
            result.setType(getProvides());
            result.setRelation(DCTERMS.CREATOR, getServiceID());  // set the service ID as provenance information for the new content part
            result.setRelation(DCTERMS.SOURCE, uri);              // set the analyzed content part as source for the new content part
            result.setProperty(DCTERMS.CREATED, isodate.format(new Date())); // set the created date for the new content part

            
            //result.setProperty(new URIImpl("http://www.mico-project.org/properties//"), Integer.toString());
           OutputStream os=result.getOutputStream();
           PrintWriter xmlOut2 = new PrintWriter(os);
           pipeline.xmlPrint(annotation, xmlOut2);

            // report newly available results to broker
            analysisResponse.sendMessage(contentItem, result.getURI());

        } catch (RepositoryException e) {
            log.error("error accessing metadata repository",e);

            throw new AnalysisException("error accessing metadata repository",e);
        }
    }


    // usage: java SentimentAnalysis <hostname> <user> <password>
    public static void main(String[] args) {
        if(args.length != 3) {
            System.err.println("Usage: java SentimentAnalysis <hostname> <user> <password>");
            System.exit(1);
        }

        String mico_host = args[0];
        String mico_user = args[1];
        String mico_pass = args[2];

        try {
            // create event manager instance, providing the correct host, user and password, and initialise it
            EventManager eventManager = new EventManagerImpl(mico_host,mico_user,mico_pass);
            eventManager.init();

            // create analyzer service instance and register it with event manager
            SentimentAnalysis svc_wc = new SentimentAnalysis();
            eventManager.registerService(svc_wc);


            // keep running service in the background, and wait for user command "q" on the frontent to terminate
            // service (other approaches might be more sensible for a service, e.g. commons-daemon)
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            char c = ' ';
            while(Character.toLowerCase(c) != 'q') {
                System.out.print("enter 'q' to quit: ");
                System.out.flush();

                c = in.readLine().charAt(0);
            }

            // unregister service before quiting
            eventManager.unregisterService(svc_wc);

            // shutdown event manager properly
            eventManager.shutdown();

            System.out.println("SentimentAnalysis shutdown completed");
            // DONE
        } catch (IOException e) {
            log.error("error while accessing event manager:",e);
        }

    }
}

    
    
    
    
    
    
    
    
    
    
    
    